#!/usr/bin/groovy

import org.example.lib.DeployUtils

def call(Map parameters = [:]) {

    def depUtils = new DeployUtils(this)

    node() {
        parallel(
            HelloWorldOne: {
                stage("Deploying HelloWorld One") {
                    depUtils.deployHelloWorld()
                    depUtils.deployWebServer()
                }
            },
            HelloWorldTwo: {
                stage("Deploying HelloWorld Two") {
                    depUtils.deployHelloWorld()
                    depUtils.deployWebServer()
                }
            }
        )
    }
}
