package org.example.lib

import org.example.lib.DockerUtils
  
public class DeployUtils implements Serializable {

    def context
    def dockerUtils

    DeployUtils(context) {
       this.context = context
       this.dockerUtils = new DockerUtils(context)
    }

    def deployHelloWorld() {
       dockerUtils.dockerRun()
    }

    def deployWebServer() {
    }
}
