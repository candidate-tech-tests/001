package org.example.lib

public class DockerUtils implements Serializable {

    def context

    DockerUtils(context) {
        this.context = context
    }

    def dockerBuild() {
    }

    def dockerPull() {
        context.sh ""
    }

    def dockerRun() {
        context.sh "docker run -d -p 80 tutum/hello-world"
    }
}

